namespace my.bookshop;

entity Books {
  key ID        : Integer;
      title     : String;
      stock     : Integer;
      publisher : Composition of many Publisher
                    on publisher.book = $self;
}

entity Publisher {
  key ID      : Integer;
      name    : String;
      book    : Association to one Books;
      offices : Composition of many Offices
                  on offices.publisher = $self;
}

entity Offices {
  key ID        : Integer;
      name      : String;
      publisher : Association to one Publisher;
}