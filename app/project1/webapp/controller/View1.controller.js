sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("project1.controller.View1", {
            onAfterRendering: function () {
                const oView = this.getView(),
                    oModel = oView.getModel(),
                oContext = oModel.createEntry("/Books", {
                    properties: { title: "Hallo" }
                })
            oView.setBindingContext(oContext)
            },
            onTableAdd: function (oEvent) {
                var oView = this.getView(),
                    oContext = oEvent.getSource().getBindingContext() || oView.getBindingContext(),
                    oTable = oEvent.getSource().getParent().getParent(),
                    oTableBinding = oTable.getBinding("items"),
                    sNavigationProperty = oTableBinding.getPath().split("/").pop(),
                    oModel = oView.getModel() // oContext.getModel().getProperty(sNavigationProperty, oContext) || 
                var sEntity = oTableBinding._getEntityType().name
                var oChildrenContext = oModel.createEntry(sNavigationProperty, { context: oContext }) // properties: { CertificateRevisionNumber: "123note" },  // sEntity
                var aChildrens = oModel.getProperty(sNavigationProperty, oContext) || []
                aChildrens.push(oChildrenContext.getPath().substring(1))//.sDeepPath.substring(1) || oChildrenContext.getPath().substring(1))
                oModel.setProperty(sNavigationProperty, aChildrens, oContext)
            }

        });
    });
