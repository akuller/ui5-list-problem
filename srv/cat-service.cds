using my.bookshop as my from '../db/data-model';

service CatalogService {
    @Core.AutoExpand
    @readonly entity Books as projection on my.Books;
}